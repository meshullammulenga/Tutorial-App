const CURRENT_YEAR = (new Date()).getFullYear();
const CURRENT_MONTH = (new Date()).getUTCMonth();
const CURRENT_DAY = (new Date()).getUTCDay();
var CURRENT_HOUR = (new Date()).getHours();
var CURRENT_MINUTE = (new Date()).getMinutes();
var CURRENT_SECOND = (new Date()).getSeconds();




export {
    CURRENT_YEAR,
    CURRENT_MONTH,
    CURRENT_DAY,
    CURRENT_HOUR,
    CURRENT_MINUTE,
    CURRENT_SECOND
}